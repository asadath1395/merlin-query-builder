const express = require('express');
const bodyParser = require('body-parser');
require('body-parser-xml')(bodyParser);
const xml2js = require('xml2js');
const uuidv1 = require('uuid/v1');
const faker = require('faker');
const fs = require('fs');
const app = express();
app.use(bodyParser.xml({
  limit: '1MB', 
  xmlParseOptions: {
    normalize: true,
    normalizeTags: true,
    explicitArray: false
  }
}));	
const port = 8000;
const base_url = "http://localhost:8000";
let start_time = null, cancelled = false, current_time, server = {}, directResponse = false;
let fakeData, page_limit, page_start, page_end;
app.post("/", (req, res)=> {
  var xml = req.body;
  var server_request_id;
  var response;
  switch(xml["merlin-request"]['$']['request-type']) {
    case "input-names":
      let inputNames = [
        {
          "calculationMethod": {
            "display-name": "Calculation Method"
          },
        },
        {
          "entities": {
            "display-name": "MaRS Entities",
            "implicit-flavors": "Both",
            "children": ["PARTICIPANT", "INST PRODUCT SUB TYPE", "PRODUCT CLASS", "PARTICIPANT MOODY RATING LT", "PARTICIPANT FITCH RATING", "POSITION PRODUCT TYPE", "FEED", "INSTRUMENT", "POSITION ID", "INSTRUMENT CLASS", "PARENT PARTICIPANT", "PARTICIPANT CSFB RATING", "PARTICIPANT SP RATING LT", "PARENT ULTIMATE PARENT", "ULTPARENT AGY RATING", "PARENT PRODUCT SUB TYPE", "ULTIMATE PARENT", "PRODUCT SUB TYPE", "PARENT POSITION"]
          }
        },
        {
          "attributes": {
            "display-name": "MaRS Attributes",
            "implicit-flavors": "Both",
            "children": ["GBM BOOK ORGANISATION", "FULL REVAL IND", "VALUATION INDICATOR", "EQ REGION", "MAS COUNTRY", "ENTITY TYPE", "COUNTRY NAME", "DELIVERY SEGMENT", "CURRENCY.EMG CURRENCIES", "CCAR COUNTRY", "ASSET REFERENCE INTERNAL", "GEOGRAPHIC REGION", "DEBT TYPE", "CURRENCY.SWAPSPREAD INFLATION", "ASSET REFERENCE", "FULL REVAL FLAG", "VOLCKER SUB DIVISION", "CVA COUNTERPARTY", "CURVE", "MBS INSTRUMENT TYPE.MBS INSTRUMENT TYPE", "CURRENCY.EURO", "CROSS TYPE", "PRODUCT TYPE", "CREDIT RATING.CREDIT RATING", "CREDIT LINE TYPE", "REGION", "COUNTRY", "COMPONENT ID", "CCAR REVAL INDICATOR", "OFFICE CODE", "CCAR REGIONAL GROUPING", "CCAR RATING", "CURRENCY.CURRENCY", "CCAR INDEX GROUPING", "INDEX", "SECURITISATION INDICATOR", "SECURITISATION INDICATOR", "ACCT METHOD", "DELIVERY MONTH", "RISK FACTOR INSTRUMENT", "LONG SHORT INDICATOR", "CCAR 14Q REVAL INDICATOR DELTA", "MDS CURVE KEY", "SCENARIO.SCENARIO", "MAS INSTRUMENT", "BUSINESS ORGANISATION.GBM EQUITIES/GLOBAL PROP", "BOND ISIN", "BASIS SWAP", "GPL REGION", "BFI LIQUIDITY ID", "DEFAULT RISK RATING", "VOL BUMP AMOUNT", "SURFACE TYPE", "EXPOSUREMETHOD", "MDS ASW CURVE", "CCAR ASSET GROUP", "CREDIT EVENT TYPE", "FV REPO HDG", "CVA SECTOR", "INFLATION INDEX", "BUSINESS ORGANISATION.BMS REGION", "CREDIT RATING.CREDIT RATING BBB AND BELOW", "RNIV TYPE", "CCAR OTHER EQ REG GROUPING", "VINTAGE CATEGORY", "TRANCHE IDENTIFIER", "MAS OFFSHORE ONSHORE INDICATOR", "AGE", "MERGER DEAL ID", "BASIS RISK TENOR", "BUY HOLD", "TRADING STRATEGY", "BENCHMARK ID", "CCAR ASSET SUB GROUP", "BIII INPUT METHOD", "CURRENCY.CURRENCY REGION", "BOND TYPE", "BFI B2 TIER", "BFI B3 TIER", "ISSUER", "BFI INDICATOR", "COMMODITY", "COLLATERAL TYPE", "BUSINESS ORGANISATION.REPORTING PACK", "FRTB ASSET CLASS", "BOOKING ENTITY GROUP", "BOOK TYPE ID", "UNDERLYING RIC CODE", "CCAR TRADING TYPE CATEGORY", "BUSINESS ORGANISATION.BUSINESS ORGANISATION", "CALL PUT", "GENERIC COUPON", "SKEW TYPE", "UNDERLYING MATURITY", "GOVT BOND SERIES", "CREDIT CLASS", "GPL ATOM CODE", "GPL BOOK GROUP LEGAL ENTITY", "MBS RATING", "MBS INSTRUMENT TYPE.MBS SECURITISATIONS", "PRICE BAND", "PROGRAM", "GPL BOOK TYPE", "MSCI SECTOR", "GPL EDLV", "CCAR COLLATERAL GROUPING", "AMOUNT TYPE", "GPL EDMRUO", "RISK CLASS", "GRANULAR RISK FACTOR", "HOLDING PERIOD", "HTM AFS INDICATOR", "INDEXSTOCK", "VOLCKER FLAG", "IMS MATURITY BUCKET", "MODEL TYPE", "PCA TYPE", "INDEX ID", "CREDIT RATING.RATING CLASS", "INDEX TYPE", "INFLATION TYPE", "INTERCOMPANY INDICATOR", "LOCAL REG TRADING", "SCENARIO FULL REVAL TYPE", "LEGAL ENTITY", "LIQUIDITY HORIZON", "LOCAL CAP CALC", "MAS COMMODITY CLASS", "DEBT PRIORITY CLASS", "MAS INSTRUMENT TYPE", "FLAG TYPE", "ISIN", "MAS PRODUCT", "CREDIT SPREAD REGION", "MAS RATING", "TRADING LOCATION", "FLAG LEVEL", "MAS SECTOR TYPE", "MAS SSIC SECTOR", "GENDER", "IHC REPO HEDGE", "MAS TENOR", "MDS CURVE", "MERGER OFFER TYPE", "ASSET TYPE", "METHODOLOGY", "OAS ID", "LEGAL LINKAGE KEY", "RATE", "REVAL INDICATOR", "SECTOR", "RISK POINT", "SCENARIO SHOCK", "SCENARIO TYPE", "SCENARIO.RNIV", "SEASONING", "LIFE ID", "BUSINESS ORGANISATION.BMS TRADER", "SECURITY TYPE", "STRATEGY", "UNDERLYING BB CODE", "FRTB LARGE CAP", "UNIT OF MEASUREMENT", "VAR PERIOD", "VOL BUMP TYPE", "ACVA SCVA EXCLUSION", "VOL CONTROL", "MBS CASH FLOW TYPE", "REPORT TYPE INDICATOR"]
          }
        },
        {
          "businessOrganisation": {
            "display-name": "Business Organisation",
            "implicit-flavors": "Both",
            "children": ["BUSINESS ORGANISATION", "BMS TRADER", "BMS REGION", "GBM EQUITIES/GLOBAL PROP", "REPORTING PACK"]
          }
        },
        {
          "riskTypes": {
            "display-name": "Risk Types",
            "implicit-flavors": "Full"
          }
        },
        {
          "parameters": {
            "display-name": "Parameters"
          }
        },
        {
          "internalCollections": {
            "display-name": "Internal Collections",
            "implicit-flavors": "Full"
          }
        },
        {
          "reportTrees": {
            "display-name": "Report Trees"
          }
        },
        {
          "compositeNodes": {
            "display-name": "Composite Nodes",
            "implicit-flavors": "Full"
          }
        },
        {
          "others": {
            "display-name": "Others",
            "children": ["nonlinear var", "VaRMethodology", "UseZeroCorrXMove", "UseRisks", "UseExtremeMove", "UseBenchMarkCorrelation", "UDMLEGALENTITY", "UDMGROUP", "UDMFORMAT", "UDMBREAKDOWN", "TickerVersionClass", "TickerVersion", "SPOTPRICECOORDINATE", "SPATIAL", "SCENARIO EXPRESSION", "REPORT DEFAULT SPREADS ONLY", "RDS_MATURITY", "ParameterVersion", "LISTBYTSTICKERIGNORESETS", "IRC_18M", "IRCSCENARIO", "IRCMULTIFACTORCONFIGID", "IMPLIEDRATINGVERSION", "HoldingPeriod", "FILE STRUCTURE", "EnableLogging", "ENFORCE CVA ALLOCATION", "ReferenceVersion", "CVAREFERENCEDATE", "CVAEXCLUDEPROXYHEDGES", "COBDate", "Legal Entity", "COBDATE", "BUSINESS ORGANISATION_1", "BUSINESS ORGANISATION1", "BUS ORG", "#Value#", "USENRSHORTPOSITIONS", "VaR Comp", "ReferenceDate", "VOLATILITY_LAMBDA", "ERCEMGTickerVersion", "UDMMODELVERSION", "TARGET BUSINESS ORGANISATION", "TickerVersionDate", "USEDARWEIGHTEDSENSITVITIES", "VOLATILITYCOORDINATE", "GPL FSA FLAG", "BMS REGION", "UseInterpolation", "UDMMODELNAME", "UseDefaultXMove", "UseBenchmarkCorrelation", "ReferenceVersionClass", "EQUITYMETHODOLOGY", "IRCMODEL", "IDRCCONFIDENCELEVEL", "CVA HEDGE BOOKS", "UseDefaultDate", "COBdate", "RISKSERVERCONNECTSTRING", "ConfidenceLevel", "CVA EE REPLACE REPORT TYPE INDICATOR"]
          }
        }
      ];
      inputs = [];
      for(let i = 0;i < inputNames.length;i++) {
        let inputName = inputNames[i];
        let inputNameKey = Object.keys(inputName)[0];
        if(inputName[inputNameKey]["implicit-flavors"]) {
          inputs.push({
            $: {
              "input-name": inputNameKey,
              "display-name": inputName[inputNameKey]["display-name"],
              "implicit-flavors": inputName[inputNameKey]["implicit-flavors"]
            }
          })
        }
        else {
          inputs.push({
            $: {
              "input-name": inputNameKey,
              "display-name": inputName[inputNameKey]["display-name"]
            }
          })
        }
        if(inputName[inputNameKey]["children"]) {
          for(let j = 0;j < inputName[inputNameKey]["children"].length;j++) {
            inputs.push({
              $: {
                "input-name": inputNameKey + "." + inputName[inputNameKey]["children"][j],
              }
            })
          }
        }
      }
      response = {
        "merlin-response": {
          $: {
            "request-type": "input-names",
            "response-type": "input-names"
          },
          input: inputs
        }
      };
      break;
    case "input-values":
      if(xml["merlin-request"]['$']['input-name'] === "composite_nodes") {
        response = {
          "merlin-response": {
            $: {
              "request-type": "input-values",
              "too-many": true
            }
          }
        };
      }
      else {
        let tree = Math.floor(Math.random() * 2);
        if(tree) {

        }
        else {

        }
        response = {"merlin-response": {$: {"request-type": "input-values"}}};
      }
      break;
    case "query-submit":
      server_request_id = uuidv1();
      response = {"merlin-response": {$: {"request-type": "query-submit", "response-type": "status", "status": "instantiated", "server-request-id": server_request_id}}};
      server[server_request_id] = {
        status: "instantiated"
      }
      start_time = new Date().getTime();
      break;
    case "query-status":
      if(!xml["merlin-request"]['$']["server-request-id"]) {
        xml = '<merlin-response request-type="query-submit" response-type="error" error-type="validation">server-request-id is required</merlin-response>';
        directResponse = true;
      }
      else {
        let serverKeyValid = true;
        Object.keys(server).map(key => {
          if(key !== xml["merlin-request"]['$']["server-request-id"]) serverKeyValid = false;
        })
        if(serverKeyValid) {
          response = {"merlin-response": {$: {"request-type": "query-status", "response-type": "status", "server-request-id": xml["merlin-request"]['$']["server-request-id"]}}};
          current_time = new Date().getTime();
          if(cancelled) {
            response["merlin-response"]["$"]["status"] = "cancelled";
            server[xml["merlin-request"]['$']["server-request-id"]] = {
              status: "cancelled"
            };
          }
          else {
            if((current_time/1000 - start_time/1000) > 30) {
              response["merlin-response"]["$"]["status"] = "completedNormally";
              server[xml["merlin-request"]['$']["server-request-id"]] = {
                status: "completedNormally"
              };
            }
            else {
              response["merlin-response"]["$"]["status"] = "running";
              server[xml["merlin-request"]['$']["server-request-id"]] = {
                status: "running"
              };
            }
          }
        }
        else {
          xml = '<merlin-response request-type="query-submit" response-type="error" error-type="validation">server-request-id is invalid</merlin-response>';
          directResponse = true;
        }
      }
      break;
    case "query-cancel":
      if(!xml["merlin-request"]['$']["server-request-id"]) {
        xml = '<merlin-response request-type="query-submit" response-type="error" error-type="validation">server-request-id is required</merlin-response>';
        directResponse = true;
      }
      else {
        let serverKeyValid = false;
        Object.keys(server).map(key => {
          if(key === xml["merlin-request"]['$']["server-request-id"]) serverKeyValid = true;
        })
        if(serverKeyValid) {
          current_time = new Date().getTime();
          response = {"merlin-response": {$: {"request-type": "query-cancel", "response-type": "status", "server-request-id": xml["merlin-request"]['$']["server-request-id"]}}};
          if((current_time/1000 - start_time/1000) < 30) {
            response["merlin-response"]["$"]["status"] = "cancelled";
            server[xml["merlin-request"]['$']["server-request-id"]] = {
              status: "cancelled"
            };
            cancelled = true;
          }
          else {
            xml = '<merlin-response request-type="query-submit" response-type="error" error-type="general">Query already executed</merlin-response>';
            directResponse = true;
          }
        }
        else {
          xml = '<merlin-response request-type="query-submit" response-type="error" error-type="validation">server-request-id is invalid</merlin-response>';
          directResponse = true;
        }
      }
      break;
    case "get-results":
      let fakeData = [];
      if(xml["merlin-request"]['$']["start-row"]) xml["merlin-request"]['$']["start-row"] = Number(xml["merlin-request"]['$']["start-row"]);
      if(xml["merlin-request"]['$']["end-row"]) xml["merlin-request"]['$']["end-row"] = Number(xml["merlin-request"]['$']["end-row"]);
      page_limit = 100;
      for(let i = 0;i < 200;i++) {
        fakeData[i] = {
          "Business Orgnisation": faker.name.findName(),
          "MaRS Script": faker.address.streetAddress(),
          "Cluster Tree": faker.phone.phoneNumber(),
          "Job Area": faker.name.jobArea(),
          "Job Descriptor": faker.name.jobDescriptor(),
          "Secondary Address": faker.address.secondaryAddress(),
          "Country": faker.address.country(),
          "Street Address": faker.address.streetAddress,
        }
      }
      response = {"merlin-response": { $: {"request-type": "get-results", "response-type": "result-table", "row-count": fakeData.length}}};
      if(xml["merlin-request"]['$']["server-request-id"]) response["merlin-response"]["$"]["server-request-id"] = xml["merlin-request"]['$']["server-request-id"];
      if(xml["merlin-request"]['$']["start-row"]) {
        page_start = xml["merlin-request"]['$']["start-row"];
        response["merlin-response"]["$"]["start-row"] = page_start;
      }
      else {
        page_start = 0;
        response["merlin-response"]["$"]["start-row"] = page_start;
      }
      if(xml["merlin-request"]['$']["end-row"] !== -1) {
        if(xml["merlin-request"]['$']["end-row"]) {
          page_end = xml["merlin-request"]['$']["end-row"];
          response["merlin-response"]["$"]["end-row"] = page_end;
        }
        else {
          page_end = page_start + page_limit
          response["merlin-response"]["$"]["end-row"] = page_end;
        }
      }
      else {
        page_end = fakeData.length - 1;
        response["merlin-response"]["$"]["end-row"] = page_end;
      }
      let tr = [], th = [];
      Object.keys(fakeData[0]).map(key => {
        th.push(key)
      });
      tr.push({th});
      for(let i = page_start;i <= page_end; i++) {
        let td = [];
        for(let j = 0;j < th.length;j++) {
          td.push(fakeData[i][th[j]])
        }
        tr.push({
          td
        })
      }
      response["merlin-response"]["table"] = {
        tr
      }
      break;
    case "download-results":
      xml = `<merlin-response request-type="download-results" response-type="success">${base_url}/download</merlin-response>`;
      directResponse = true;
      break;
    case "save-query":
      response = {"merlin-response": {$: {"request-type": "save-query", "response-type": "success"}}};
      break;
    case "load-query":
      let queries = [], randomQuery = Math.floor(Math.random() * 10);
      queries.push('<query query-type="report-tree" validate="true"><statement><term input-name="compositeNodes"><like pattern="*_MACRO"/></term></statement><statement><term input-name="businessOrganisation.BUSINESS ORGANISATION"><equals value="GS_DIV_GSGF"/></term></statement><statement><term input-name="attributes.CURRENCY.CURRENCY"><equals value="AUD"/></term></statement></query>');
      queries.push('<query query-type="reorg" validate="true" cob-date="20180529"><statement><term input-name="businessOrganisation.BUSINESS ORGANISATION" implicit-search="Full" levels-down="2"><equals value="FBC_12CR"/><equals value="FBC_P602"/><equals value="FBC_P610"/><equals value="FBC_P646"/><equals value="FBC_P655"/><equals value="GS_CLS_E2CM_DIVHIER"/><equals value="GS_CLS_ECT2_DIVHIER"/><equals value="GS_CLS_ELVJ"/><equals value="GS_PL_MRTG"/><equals value="GS_PL_WSQ2"/><equals value="GS_PL2_C148"/><equals value="GS_PL2_C154"/><equals value="GS_PL2_C155"/><equals value="GS_PL2_C156"/><equals value="GS_PL2_C167"/><equals value="GS_PL2_CRIO"/><equals value="GS_PL2_D1ET"/></term></statement><output-scope outscope="mars"/><output-scope outscope="clusternet"/><additional-breakdown input-name="internalCollections"/></query>');
      queries.push('<query query-type="mars-schedules" validate="true"><statement><term input-name="businessOrganisation.BUSINESS ORGANISATION" implicit-search="Hierarchy" levels-down="2"><equals value="FBC_12CR"/><equals value="FBC_P602"/><equals value="FBC_P610"/><equals value="FBC_P646"/><equals value="FBC_P655"/><equals value="GS_CLS_E2CM_DIVHIER"/><equals value="GS_CLS_ECT2_DIVHIER"/><equals value="GS_CLS_ELVJ"/><equals value="GS_PL_MRTG"/><equals value="GS_PL_WSQ2"/><equals value="GS_PL2_C148"/><equals value="GS_PL2_C154"/><equals value="GS_PL2_C155"/><equals value="GS_PL2_C156"/><equals value="GS_PL2_C167"/><equals value="GS_PL2_CRIO"/><equals value="GS_PL2_D1ET"/></term></statement></query>')
      queries.push('<query query-type="internal-collections"><statement><term input-name="internalCollections"><equals value="NET_FX"/></term></statement><statement><term input-name="riskTypes"><equals value="FXDELTA"/></term></statement><statement><term input-name="attributes.VALUATION INDICATOR"><equals value="MIS"/></term></statement></query>')
      queries.push('<query query-type="impact" validate="true" cob-date="20180529"><statement><term input-name="riskTypes"><equals value="FXDELTA"/><equals value="MTMVALUE"/><equals value="FXGAMMA"/></term></statement><output-scope outscope="mars"/><output-scope outscope="clusternet"/><additional-breakdown input-name="calculationMethod"/></query>')
      queries.push('<query query-type="impact" validate="true" cob-date="20180529" script-list-name="Karishma"><statement><term input-name="businessOrganisation.BUSINESS ORGANISATION" implicit-search="Hierarchy" levels-up="2"><equals value="GS_TOT_DVTD"/><equals value="GS_TOT_FMMT"/><equals value="SB_TOT_CHUB"/></term><term input-name="attributes.LEGAL ENTITY"><equals value="LEGAL ENTITY"/></term></statement><statement><term input-name="riskTypes" implicit-search="Full"><equals value="FXDELTA"/><equals value="MTMVALUE"/><equals value="FXGAMMA"/></term></statement><statement><term input-name="attributes.CREDIT RATING.CREDIT RATING BBB AND BELOW"><equals value="BBB"/><equals value="CCC"/></term></statement><output-scope outscope="mars"/><output-scope outscope="clusternet"/><additional-breakdown input-name="calculationMethod"/></query>')
      queries.push('<query query-type="internal-collections-expanded"><statement><term input-name="internalCollections"><equals value="TOTAL_VAR"/></term></statement></query>')
      queries.push('<query query-type="flags-and-limits" validate="true"><statement><term input-name="businessOrganisation.BUSINESS ORGANISATION" implicit-search="Hierarchy" levels-up="2"><equals value="GS_TOT_DVTD"/><equals value="GS_TOT_FMMT"/><equals value="SB_TOT_CHUB"/></term></statement><input-scope inscope="non-tactical-limit"/><input-scope inscope="tactical-limit"/></query>')
      queries.push('<query query-type="composite-node" validate="true"><statement><term input-name="businessOrganisation.BUSINESS ORGANISATION"><like pattern="*_DIVHIER"/></term></statement><statement><term input-name="attributes.CURRENCY.CURRENCY"><equals value="GBP"/></term></statement></query>')
      queries.push('<query query-type="ancestral"><statement><term input-name="entities.INST PRODUCT SUB TYPE"><equals value="LOAN_T"/></term></statement></query>')
      xml = `<merlin-response request-type="load-query" response-type="load-query">${queries[randomQuery]}</merlin-response>`;
      directResponse = true;
      break;
    case "rm-query":
      response = {"merlin-response": {$: {"request-type": "rm-query", "response-type": "success"}}};
      break;
    case "ls-query":
      let fakeQueryData = [], keys = [];
      response = {};
      page_limit = 30;
      if(xml["merlin-request"]['$']["start-row"]) xml["merlin-request"]['$']["start-row"] = Number(xml["merlin-request"]['$']["start-row"])
      if(xml["merlin-request"]['$']["end-row"]) xml["merlin-request"]['$']["end-row"] = Number(xml["merlin-request"]['$']["end-row"])
      for(let i = 0;i < 100;i++) {
        fakeQueryData[i] = {
          "query-type": faker.name.jobArea(),
          "query-name": faker.name.jobTitle(),
          "user-name": faker.name.findName(),
          "timestamp": faker.random.uuid()
        }
      }
      Object.keys(fakeQueryData[0]).map(key => {
        keys.push(key)
      })
      if(xml["merlin-request"]['$']["start-row"]) page_start = xml["merlin-request"]['$']["start-row"];
      else page_start = 0;
      if(xml["merlin-request"]['$']["end-row"] !== -1) {
        if(xml["merlin-request"]['$']["end-row"]) page_end = xml["merlin-request"]['$']["end-row"];
        else page_end = page_start + page_limit
      }
      else {
        page_end = fakeQueryData.length - 1;
      }
      let query = [], savedQuery = [];
      for(let i = page_start;i <= page_end;i++) {
        let temp = {};
        for(let j = 0;j < keys.length;j++) {
          temp[keys[j]] = fakeQueryData[i][keys[j]]
        }
        savedQuery[i] = {
          $: temp
        }
      }
      response["merlin-response"] = {
        "$": {
          "request-type": "ls-query",
          "response-type": "ls-query",
          "start-row": page_start,
          "end-row": page_end,
          "row-count": fakeQueryData.length
        },
        "saved-query": savedQuery
      }
      break;
    case "query-sql":
      let sql = "\tSELECT DISTINCT r.report_name, v.attribute_value, vt.value_type \r\n \t\tFROM mlreportdefinition r \r\n \t\tJOIN MRLDEFINITIONONTAINER dct ON r-report_defintion_id= dct.report_defintion_id \r\n \t\tJOIN MRLDEFINTIONCONTAINERMAP cm ON dct.container_id = cm.container_id\r\n\t\tJOIN MRLCONTAINERTYPE ct ON dct.container_type = ct.type_id\r\n\t\tLEFT JOIN mrlattributevaluegroup ag ON cm.attribute_group_id = ag.attribute_group_id\r\n\t\tLEFT JOIN MRLATTRIBUTEVALUE v ON ag.attribute_value_id = v.attribute_value_id\r\n\t\tLEFT JOIN mrlattributevaluetype vt ON ag.attribute_value_type = vt.value_type_id\r\n\t\tWHERE r.closed_date IS NULL\r\n\t\t\tAND sysdate >= r.begin_cob_date\r\n\t\t\tAND sysdate < r.end_cob_date\r\n\t\t\tAND v.attribute_value IN ('IRVEGA', 'IRNORMALVEGA)\r\n\t\tUNION";
      xml = `<merlin-response request-type="query-sql"><![CDATA[\r\n${sql}\r\n]]></merlin-response>`
      directResponse = true;
      break;
    case "create-script-list":
      response = {"merlin-response": {$: {"request-type": "create-script-list", "response-type": "success"}}};
      break;
    case "rm-script-list":
      response = {"merlin-response": {$: {"request-type": "rm-script-list", "response-type": "success"}}};
      break;
    case "load-script-list":
      let randomValue = Math.floor((Math.random() * 16) + 4), script = [];
      for(let i = 0;i < randomValue;i++) {
        script[i] = {
          $: {
            "script-name": faker.name.findName()
          }
        }
      }
      response = {
        "merlin-response": {
          $: {
            "request-type": "load-script-list",
            "response-type": "load-script-list"
          },
          script
        }
      }
      break;
    case "edit-script-list":
      response = {"merlin-response": {$: {"request-type": "edit-script-list", "response-type": "success"}}};
      break;
    case "ls-script-list":
      let value = Math.floor((Math.random() * 16) + 4), scriptList = [];
      for(let i = 0;i < value;i++) {
        scriptList[i] = {
          $: {
            "script-list-name": faker.name.findName()
          }
        }
      }
      response = {
        "merlin-response": {
          $: {
            "request-type": "ls-script-list",
            "response-type": "ls-script-list",
            "user-name": faker.name.firstName(),
            "last-modified": faker.name.lastName(),
            "script-count": value
          },
          "script-list": scriptList
        }
      }
      break;
    case "server-stats":
      let stats = [
        {
          $: {
            key: "total-scripts-populated",
            value: "68%"
          }
        },
        {
          $: {
            key: "total-reports-populated",
            value: "75%",
          }
        },
        {
          $: {
            key: "total-lists-created",
            value: "50",
          }
        },
        {
          $: {
            key: "total-templates-created",
            value: "50",
          }
        }
      ]
      response = {
        "merlin-response": {
          $: {
            "request-type": "server-stats",
            "response-type": "server-stats",
          },
          "server-stat": stats
        }
      }
      break;
    default:
      xml = `<merlin-response request-type="${xml["merlin-request"]['$']["request-type"]}" response-type="error" error-type="general">Server encountered some error</merlin-response>`;
      directResponse = true;
      break;
  }
  if(!directResponse) {
    var builder = new xml2js.Builder();
    var xml = builder.buildObject(response);
  }
  directResponse = false;
  res.send(xml);
})

app.get("/download", (req, res) => {
  fakeData = "";
  fakeData += "Business Organisation, MaRS Script, Cluster Tree, Job Area, Job Descriptor, Secondary Address, Country, Street Address";
  fakeData += "\r\n";
  for(let i = 0;i < 100;i++) {
    fakeData += `${faker.name.findName()}, ${faker.address.streetAddress()}, ${faker.phone.phoneNumber()}, ${faker.name.jobArea()}, ${faker.name.jobDescriptor()}, ${faker.address.secondaryAddress()}, ${faker.address.country()}, ${faker.address.streetAddress()}`;
    fakeData += "\r\n";
  }
  res.setHeader('Content-disposition', 'attachment; filename=data.csv');
  res.set('Content-Type', 'text/csv');
  res.status(200).send(fakeData);
})

app.listen(port, () => {
  console.log('Server running on port ' + port);
});